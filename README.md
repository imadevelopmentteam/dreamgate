## DreamGate Custom Install files for Firestorm Viewer  
---

This repository includes xml and png file changes to Firestorm for OpenSimulator used to make a custom installer dubbed DreamGate. These configuration changes were made to meet the needs of new users on mid-range laptops and low-end desktops wanting to use the Firestorm viewer. Specific use cases included those attending events with livestream presentations, who had visual or cognitive needs not met with the default skin, and/or who had difficulty installing Firestorm. Changing certain preferences and the default skin resolved these issues for these users.  

---
  
## Based on Firestorm for OpenSimulator  
  
https://www.firestormviewer.org/os-operating-system/  
  
Copyrights retained by the Firestorm Project and Linden Labs respectively.  
  
https://wiki.firestormviewer.org/firestorm_licenses  
  
http://wiki.secondlife.com/wiki/Linden_Lab_Official:Second_Life_Viewer_Licensing_Program  
  
http://wiki.secondlife.com/wiki/API_Terms_of_Use  
  
  
## File Types  
  
xml  
png  
  
See instructions for details  
  
No known bugs  
  
  
## Repository License and Copyright    
  
License: GNU Lesser General Public License, version 2.1  
  
Copyright 2021, Infinite Metaverse Alliance® LLC (IMA), All rights reserved. 
  
Infinite Metaverse Alliance® is a registered U.S. Trademark.  

